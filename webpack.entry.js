var core = require('itform-core');

function initializeRegistry(registry) {
  registry.register(require('itform-core/components/carDetailSteps'));
  registry.register(require('itform-core/components/carCost'));
  registry.register(require('itform-core/components/drivers'));
  registry.register(require('itform-core/components/credit'));
  registry.register(require('itform-core/components/show'));
  registry.register(require('itform-core/components/simple'));
  registry.register(require('itform-core/components/program'));
  registry.register(require('itform-core/components/preload'));
  registry.register(require('itform-core/components/extraParameters'));
  registry.register(require('itform-core/components/notification'));
  registry.register(require('itform-core/components/itProgramMessages'));
  registry.register(require('itform-core/components/defaultOrder'));
}

function kupikaskoForm(elements) {
  var form = Object.create({
    HTTP_HOST: 'http://enter.b2bpolis.ru/',
    HTTP_MAIL_URL: 'http://expert-car.ru/kasko/mail.php',
    DEFAULT_REGION: 30261 //Moscow
  });

  form.registry = core.baseRegistry();
  initializeRegistry(form.registry);
  form.factory = core.defaultFactory(form.registry);

  core.features.coreForm(form);
  core.features.xhrProxy(form, 'http://expert-car.ru/kasko/proxy.php');
  core.features.sequentialSteps(form);
  core.features.stepCleanups(form);
  core.features.stepDependencies(form);

  form.model = core.initModel(elements, form);
  form.callAfterInit();
  return form;
}

module.exports = kupikaskoForm;
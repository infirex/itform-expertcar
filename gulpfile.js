var gulp = require('gulp');
var processhtml = require('gulp-processhtml');
var less = require('gulp-less');
var gutil = require('gulp-util');
var open = require("open");
var livereload = require('gulp-livereload');
var plumber = require('gulp-plumber');
var spritesmith = require('gulp.spritesmith');
const imagemin = require('gulp-imagemin');
var clean = require('gulp-clean');

var webpack = require('webpack');
var WebpackDevServer = require('webpack-dev-server');

var webpackConfig = require('./webpack.conf');

function handleError(err) {
  console.error(err);
  this.emit('end');
}

gulp.task('clean', function() {
  return gulp.src('dist', {read: false})
  .pipe(clean());
});

gulp.task('sprite', function() {
  var spriteData = gulp.src('images/sprite/*.png').pipe(spritesmith({
    imgName: 'images/sprite.png',
    cssName: 'sprite.css'
  }));
  return spriteData.pipe(gulp.dest('dist'));
});

gulp.task('images', function() {
  return gulp.src('images/*.{gif,jpg,png}')
  .pipe(imagemin())
  .pipe(gulp.dest('dist/images'));
});

gulp.task('html', function() {
  return gulp.src('./src/html/itform.html')
  .pipe(processhtml({}))
  .pipe(gulp.dest('dist'))
  .pipe(livereload());
});

gulp.task('bootstrap', function() {
  return gulp.src('./src/bootstrap-3.0.0/bootstrap.less')
  .pipe(plumber({errorHandler: handleError}))
  .pipe(less())
  .pipe(gulp.dest('dist'))
  .pipe(livereload());
});

gulp.task('style', function() {
  return gulp.src('./src/less/build.less')
  .pipe(plumber({errorHandler: handleError}))
  .pipe(less())
  .pipe(gulp.dest('dist'))
  .pipe(livereload());
});

gulp.task('webpack', function(callback) {
  webpackConfig.plugins = [
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.optimize.UglifyJsPlugin()
  ];
  
  webpack(webpackConfig, function(err, stats) {

    if (err) throw new gutil.PluginError("webpack", err);
    gutil.log("[webpack]", stats.toString({
      // output options
    }));
    livereload.reload();
    callback();
  });
});

gulp.task("server", function(callback) {
  var config = Object.assign({}, webpackConfig, {
    devtool: 'inline-source-map'
  });
  // config.output.devtoolModuleFilenameTemplate = 'webpack://[resource-path]';

  var compiler = webpack(config);

  new WebpackDevServer(compiler, {
    contentBase: './dist',
    noInfo: 'true'
  }).listen(8080, "0.0.0.0", function(err) {
    if (err) throw new gutil.PluginError("webpack-dev-server", err);
    // Server listening
    gutil.log("[webpack-dev-server]",
      "http://localhost:8080/");
    // keep the server alive or continue?
    callback();
  });
});

gulp.task('watch', ['build'], function() {
  livereload.listen();
  gulp.watch('./src/bootstrap-3.0.0/**/*.less', ['bootstrap']);
  gulp.watch('./src/less/**/*.less', ['style']);
  gulp.watch('./src/html/**/*.html', ['html']);
  gulp.watch('./images/sprite/*', ['sprite']);
  gulp.watch('./images/*.{gif,jpg,png}', ['images']);
});

gulp.task('build', ['style', 'bootstrap', 'images', 'sprite', 'html']);

gulp.task('dev', ['watch', 'server']);
